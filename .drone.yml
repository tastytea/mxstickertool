# SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
#
# SPDX-License-Identifier: CC0-1.0

name: Build x86_64
kind: pipeline
type: docker

trigger:
  event:
    exclude:
    - tag

steps:
- name: GCC 10 / clang 11
  image: debian:bullseye-slim
  pull: always
  environment:
    CXX: g++-10
    CXXFLAGS: -pipe -O2
    DEBIAN_FRONTEND: noninteractive
    LANG: C.UTF-8
  commands:
  - CXXFLAGS="${CXXFLAGS} -Wno-null-dereference" # NOTE: temporary workaround, see commit
  - /bin/echo -e 'Package\x3a *\nPin\x3a release a=bullseye-backports\nPin-Priority\x3a 50' >> /etc/apt/preferences.d/backports-lowprio
  - echo 'deb http://deb.debian.org/debian bullseye-backports main' >> /etc/apt/sources.list.d/backports.list
  - apt-get update -q
  - alias aptinstall='apt-get install -qq --no-install-recommends'
  - aptinstall build-essential pkg-config cmake git clang locales ca-certificates # build system dependencies
  - aptinstall -t bullseye-backports meson
  - aptinstall libspdlog-dev nlohmann-json3-dev # program dependencies
  - aptinstall libcurl4-openssl-dev libssl-dev libre2-dev libevent-dev # mtxclient dependencies
  - aptinstall -t bullseye-backports libolm-dev
  - aptinstall libarchive-dev qtbase5-dev # qarchive dependencies
  - meson setup build_gcc --werror -Dargparse:werror=false -Dcoeurl:werror=false -Dfmt:werror=false -Dmtxclient:werror=false -Dnlohmann_json:werror=false -Dqarchive:werror=false -Dspdlog:werror=false
  - meson compile -C build_gcc --verbose --jobs 1
  - meson install -C build_gcc --destdir=install
  - CXX="clang++" meson setup build_gcc --werror -Dargparse:werror=false -Dcoeurl:werror=false -Dfmt:werror=false -Dmtxclient:werror=false -Dnlohmann_json:werror=false -Dqarchive:werror=false -Dspdlog:werror=false
  - meson compile -C build_clang --verbose --jobs 1
  - meson install -C build_clang --destdir=install

- name: notify
  image: drillster/drone-email
  pull: always
  settings:
    host: mail.tzend.de
    from: drone@tzend.de
    username:
      from_secret: email_username
    password:
      from_secret: email_password
  when:
    status: [ changed, failure ]
  depends_on:
    - GCC 10 / clang 11

# vim: set ts=2 sw=2 et tw=1000 ft=yaml:
