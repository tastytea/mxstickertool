# SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
#
# SPDX-License-Identifier: CC0-1.0

project(
  'mxstickertool',
  'cpp',
  version: '0.0.0',
  license: 'AGPL-3.0-only',
  default_options: ['cpp_std=c++20', 'warning_level=3'],
  meson_version: '>=0.60.0'
)

compiler = meson.get_compiler('cpp')

if get_option('buildtype').startswith('debug')
  warn_args = []
  foreach arg: [
    '-Wuninitialized',
    '-Wshadow',
    '-Wnon-virtual-dtor',
    '-Wconversion',
    '-Wsign-conversion',
    '-Wold-style-cast',
    '-Wzero-as-null-pointer-constant',
    '-Wmissing-declarations',
    '-Wcast-align',
    '-Wunused',
    '-Woverloaded-virtual',
    '-Wdouble-promotion',
    '-Wformat=2',
    '-Wlogical-op',
    '-Wuseless-cast',
    '-Wmisleading-indentation',
    '-Wduplicated-cond',
    '-Wnull-dereference',
    '-Wduplicated-branches'
  ]
    if compiler.has_argument(arg)
      warn_args += arg
    endif
  endforeach

  debug_args = warn_args
  foreach arg: ['-ftrapv']
    if compiler.has_argument(arg)
      debug_args += arg
    endif
  endforeach

  add_project_arguments(debug_args, language: 'cpp')
endif

argparse_dep = dependency('argparse', version: '>=2.8', include_type: 'system')
mtxclient_dep = dependency(
  ['mtxclient', 'MatrixClient'],
  version: '>=0.8.0',
  include_type: 'system'
)
json_dep = dependency(
  'nlohmann_json',
  version: '>=3.2.0',
  include_type: 'system'
)
spdlog_dep = dependency('spdlog', version: '>=1.8.0', include_type: 'system')
qarchive_dep = dependency(
  'QArchive',
  version: '>=2.0.0',
  include_type: 'system'
)
fmt_dep = dependency('fmt', version: '>=5.0.0', include_type: 'system')

qt_dep = dependency(
  'qt6',
  modules: ['Core'],
  include_type: 'system',
  required: get_option('qt6')
)
if not qt_dep.found()
  qt_dep = dependency('qt5', modules: ['Core'], include_type: 'system')
endif

subdir('src')
