// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "config.hpp"

#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>

#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <stdexcept>
#include <string_view>

namespace mxstickertool::config {

config read(const std::string_view mxid) {
    config cfg;
    std::ifstream file(get_config_path());

    nlohmann::json json{nlohmann::json::parse(file).at(mxid.data())};
    json = json[0]; // FIXME: ???

    cfg.device_id = json.at("device_id");
    cfg.access_token = json.at("access_token");
    spdlog::debug("config file read from {:s}", get_config_path().c_str());

    return cfg;
}

void write(const std::string_view mxid, const config &cfg) {
    std::string config_path{get_config_path()};
    std::ifstream infile(config_path);
    nlohmann::json json{nlohmann::json::parse(infile)};
    infile.close();

    // FIXME: Why the [0] all of a sudden?
    json[0][mxid.data()] = {
        {"device_id",    cfg.device_id   },
        {"access_token", cfg.access_token}
    };
    spdlog::debug(json.dump(4));

    std::ofstream outfile(config_path);
    outfile << json[0].dump(4);
    spdlog::debug("config file written to {:s}", get_config_path().c_str());
}

std::string_view get_env(const std::string_view name) {
    const char *env{std::getenv(name.data())}; // NOLINT(concurrency-mt-unsafe)
    if (env != nullptr) {
        spdlog::debug("environment variable {:s} exists and is {:s}", name,
                      env);
        return env;
    }
    spdlog::debug("environment variable {:s} does not exist", name);

    return {};
}

std::filesystem::path get_config_path() {
    std::filesystem::path path{get_env("XDG_CONFIG_HOME")};
    if (path.empty()) {
        path = get_env("HOME");
        if (!path.empty()) {
            path /= ".config";
        }
    }
    if (!path.empty()) {
        if (!std::filesystem::exists(path / "mxstickertool")) {
            std::filesystem::create_directory(path /= "mxstickertool");
        }

        return path / "mxstickertool" / "mxstickertool.json";
    }

    throw std::runtime_error{"could not determine config file location"};
}

} // namespace mxstickertool::config
