// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "archive.hpp"
#include "matrix.hpp"
#include "options.hpp"

#include <spdlog/common.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include <exception>

int main(int argc, char *argv[]) {
    using namespace mxstickertool;

    spdlog::set_pattern("%v");
    spdlog::set_default_logger(spdlog::stderr_color_st("mxstickertool"));

    try {
        options::options opts{options::parse_options(argc, argv)};
        if (opts.debug) {
            spdlog::set_level(spdlog::level::debug);
            spdlog::set_pattern("[%^%n%$] [%^%l%$] %v");
        } else {
            spdlog::set_pattern("%v");
        }

        spdlog::debug("got options:\n  mxid: {:s}\n  room: {:s}\n  pack: {:s}\n"
                      "  rate: {:d}\n  output: {:s}\n  input: {:s}",
                      opts.mxid, opts.roomid, opts.packid, opts.rate,
                      opts.output_file, opts.input_file);

        switch (opts.command) {
            case options::cmd::download: matrix::download(opts); break;
            case options::cmd::upload: matrix::upload(opts); break;
        }
    } catch (const std::exception &err) {
        spdlog::error("{:s}", err.what());
        return 1;
    }
}
