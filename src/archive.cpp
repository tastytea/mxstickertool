// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "archive.hpp"

#include "helpers.hpp"

#include <fmt/format.h>
#include <QArchive>
#include <QCoreApplication>
#include <QObject>
#include <spdlog/spdlog.h>

#include <cstdint>
#include <filesystem>
#include <stdexcept>
#include <string_view>

namespace mxstickertool::archive {

void write(const format format, const std::string_view filename,
           const std::vector<std::filesystem::path> &files,
           const std::string_view basedir) {
    // Doesn't work without a QCoreApplication
    int argc{0};
    char *argv[]{nullptr};
    QCoreApplication app(argc, argv);

    QArchive::DiskCompressor compressor(filename.data());
    // NOLINTNEXTLINE(google-runtime-int)
    compressor.setArchiveFormat(static_cast<short>(format));

    for (const auto &file : files) {
        std::string entry{file};
        entry = entry.substr(basedir.size() + 1);
        compressor.addFiles(entry.data(), file.c_str());
    }

    QObject::connect(&compressor, &QArchive::DiskCompressor::finished, []() {
        spdlog::debug("archive successfully created");
        QCoreApplication::quit();
    });

    QObject::connect(&compressor, &QArchive::DiskCompressor::error,
                     // NOLINTNEXTLINE(google-runtime-int)
                     [](const short code, const QString &archive_filename) {
                         throw std::runtime_error{fmt::format(
                             "error creating archive: {:s} ({:s})",
                             QArchive::errorCodeToString(code).toStdString(),
                             archive_filename.toStdString())};
                         QCoreApplication::quit();
                     });

    compressor.start();
    QCoreApplication::exec();

    spdlog::info("{:s} written", filename);
}

std::string extract(const std::string_view filename) {
    // Doesn't work without a QCoreApplication
    int argc{0};
    char *argv[]{nullptr};
    QCoreApplication app(argc, argv);

    const auto tmpdir{helpers::make_tempdir()};
    QArchive::DiskExtractor extractor(filename.data(), tmpdir.c_str());

    QObject::connect(&extractor, &QArchive::DiskExtractor::finished, []() {
        spdlog::debug("extracted files successfully");
        QCoreApplication::quit();
        return;
    });

    QObject::connect(&extractor, &QArchive::DiskExtractor::error,
                     [](const short code) { // NOLINT(google-runtime-int)
                         throw std::runtime_error{fmt::format(
                             "could not extract archive: {:s}",
                             QArchive::errorCodeToString(code).toStdString())};
                     });

    extractor.start();
    QCoreApplication::exec();

    return tmpdir;
}

} // namespace mxstickertool::archive
