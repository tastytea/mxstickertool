// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#ifndef MXSTICKERTOOL_MATRIX_HPP
#define MXSTICKERTOOL_MATRIX_HPP

#include "options.hpp"

#include <mtx/responses/sync.hpp>
#include <mtxclient/http/client.hpp>

namespace mxstickertool::matrix {

//! download pack
void download(const options::options &opts);

//! upload pack
void upload(const options::options &opts);

//! authenticate do inital sync with `initial_sync_handler`
void connect();

//! do initial sync and join room if necessary
void initial_sync_handler(const mtx::responses::Sync &res,
                          mtx::http::RequestErr err);

} // namespace mxstickertool::matrix

#endif // MXSTICKERTOOL_MATRIX_HPP
