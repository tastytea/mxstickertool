// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#ifndef MXSTICKERTOOL_CONFIG_HPP
#define MXSTICKERTOOL_CONFIG_HPP

#include <filesystem>
#include <string>
#include <string_view>

namespace mxstickertool::config {

//! data from the config file
struct config {
    std::string device_id;
    std::string access_token;
};

//! read config file and return contents
config read(std::string_view mxid);

//! write config file
void write(std::string_view mxid, const config &cfg);

//! get environment variable
std::string_view get_env(std::string_view name);

//! get path of configuration file
std::filesystem::path get_config_path();

} // namespace mxstickertool::config

#endif // MXSTICKERTOOL_CONFIG_HPP
