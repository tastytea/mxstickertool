// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "options.hpp"

#include "version.hpp"

#include <argparse/argparse.hpp>
#include <fmt/core.h>
#include <spdlog/spdlog.h>

#include <cstdint>
#include <filesystem>
#include <iostream>
#include <limits>
#include <memory>
#include <stdexcept>
#include <string>

namespace mxstickertool::options {

options parse_options(int argc, char **argv) {
    argparse::ArgumentParser program("mxstickertool", version.data(),
                                     argparse::default_arguments::help);
    program.add_argument("--version", "-V")
        .help("show version and license information")
        .implicit_value(true)
        .default_value(false)
        .nargs(0)
        .action([=](const std::string &) {
            std::cout << "mxstickertool " << version << '\n'
                      << "Copyright © 2022 tastytea <tastytea@tastytea.de>\n"
                         "License AGPL-3.0-only "
                         "<https://gnu.org/licenses/agpl.html>.\n"
                         "This program comes with ABSOLUTELY NO WARRANTY. This "
                         "is free software,\n"
                         "and you are welcome to redistribute it under "
                         "certain conditions.\n";
            std::exit(0); // NOLINT concurrency-mt-unsafe
        });
    program.add_argument("--debug")
        .help("enable debug output")
        .implicit_value(true)
        .default_value(false);

    argparse::ArgumentParser command("command", "",
                                     argparse::default_arguments::none);
    command.add_argument("--mxid")
        .help("Matrix ID to login with (@user:server)")
        .required();
    command.add_argument("--roomid")
        .help("download/upload sticker pack from/to this room")
        .required();
    command.add_argument("--packid").help("ID of the sticker pack").required();
    command.add_argument("--rate")
        .help("download/upload this many images per minute")
        .scan<'d', std::uint16_t>()
        .default_value(std::uint16_t{60});

    argparse::ArgumentParser download("download", "",
                                      argparse::default_arguments::help);
    download.add_description("download sticker pack");
    download.add_parents(command);
    download.add_argument("--output-file")
        .help("file to save the sticker pack in");
    program.add_subparser(download);

    argparse::ArgumentParser upload("upload", "",
                                    argparse::default_arguments::help);
    upload.add_description("upload sticker pack");
    upload.add_parents(command);
    upload.add_argument("--input-file").help("sticker pack to upload");
    program.add_subparser(upload);

    options opts;
    std::unique_ptr<argparse::ArgumentParser> parser;
    try {
        program.parse_args(argc, argv);
        if (program.is_subcommand_used("download")) {
            opts.command = cmd::download;
            parser = std::make_unique<argparse::ArgumentParser>(download);
        } else if (program.is_subcommand_used("upload")) {
            opts.command = cmd::upload;
            parser = std::make_unique<argparse::ArgumentParser>(upload);
        } else {
            throw std::runtime_error{"download or upload are required"};
        }
    } catch (const std::runtime_error &err) {
        spdlog::error("argument error: {:s}", err.what());
        std::cerr << program;
        std::exit(1); // NOLINT(concurrency-mt-unsafe)
    } catch (const std::invalid_argument &err) {
        throw std::runtime_error{"--rate must be between 1 and 60000"};
    }

    opts.debug = program.get<bool>("--debug");
    opts.mxid = parser->get<std::string>("--mxid");
    opts.roomid = parser->get<std::string>("--roomid");
    opts.packid = parser->get<std::string>("--packid");
    opts.rate = parser->get<std::uint16_t>("--rate");
    if (program.is_subcommand_used("download")
        && parser->present("--output-file")) {
        opts.output_file = parser->get<std::string>("--output-file");
        if (std::filesystem::exists(opts.output_file.data())) {
            throw std::runtime_error{fmt::format(
                "output file {:s} already exists", opts.output_file.data())};
        }
    }
    if (program.is_subcommand_used("upload")
        && parser->present("--input-file")) {
        opts.input_file = parser->get<std::string>("--input-file");
        if (!std::filesystem::exists(opts.input_file.data())) {
            throw std::runtime_error{fmt::format(
                "input file {:s} does not exist", opts.input_file.data())};
        }
    }

    if (opts.mxid.find('@') == std::string::npos
        || opts.mxid.find(':') == std::string::npos) {
        throw std::runtime_error{"--mxid must contain @ and :"};
    }

    if (opts.rate < 1) {
        spdlog::warn("setting --rate from {:d} to 1", opts.rate);
        opts.rate = 1;
    }
    if (opts.rate > 60'000) {
        spdlog::warn("setting --rate from {:d} to 60000", opts.rate);
        opts.rate = 60'000;
    }

    if (opts.command == cmd::download && opts.output_file.empty()) {
        throw std::runtime_error{"--output-file: required"};
    }
    if (opts.command == cmd::upload && opts.input_file.empty()) {
        throw std::runtime_error{"--input-file: required"};
    }

    return opts;
}

} // namespace mxstickertool::options
