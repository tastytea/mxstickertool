// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "matrix.hpp"

#include "archive.hpp"
#include "config.hpp"
#include "helpers.hpp"
#include "options.hpp"

#include <fmt/core.h>
#include <mtx.hpp>
#include <mtx/events/event_type.hpp>
#include <mtx/events/mscs/image_packs.hpp>
#include <mtx/log.hpp>
#include <mtx/responses/common.hpp>
#include <mtx/responses/sync.hpp>
#include <mtxclient/http/client.hpp>
#include <nlohmann/json.hpp>
#include <spdlog/common.h>
#include <spdlog/spdlog.h>
#include <unistd.h>

#include <chrono>
#include <cstdint>
#include <cstdlib>
#include <exception>
#include <filesystem>
#include <fstream>
#include <ios>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

namespace mxstickertool::matrix {

using namespace std::chrono_literals;

struct global {
    std::shared_ptr<mtx::http::Client> client{nullptr};
    options::options opts;
    bool connected{false};
    bool synced{false};
} global;

void download(const options::options &opts) {
    namespace fs = std::filesystem;

    global.opts = opts;
    connect();

    while (!global.synced) {
        std::this_thread::sleep_for(100ms);
    }

    spdlog::debug("getting the state event for the pack…");
    mtx::events::msc2545::ImagePack pack;
    bool received{false};
    global.client->get_state_event<mtx::events::msc2545::ImagePack>(
        opts.roomid, opts.packid,
        [&pack, &received](const mtx::events::msc2545::ImagePack &packevent,
                           mtx::http::RequestErr &err) {
            if (err) {
                throw std::runtime_error{fmt::format(
                    "error retrieving pack: {:s}", err->matrix_error.error)};
                return;
            }
            pack = packevent;
            received = true;
        });
    while (!received) {
        std::this_thread::sleep_for(100ms);
    }

    const auto tmpdir{helpers::make_tempdir()};
    fs::create_directory(tmpdir / opts.packid);

    spdlog::info("downloading images from pack {:s} ({:s})…", opts.packid,
                 pack.pack ? pack.pack->display_name : "[unknown]");
    std::vector<fs::path> packfiles;
    for (const auto &img : pack.images) {
        bool downloaded{false};
        global.client->download(
            img.second.url, [&tmpdir, &img, &downloaded,
                             &packfiles](const std::string &data,
                                         const std::string & /*content_type*/,
                                         const std::string &original_filename,
                                         mtx::http::RequestErr &err) {
                spdlog::info("downloading {:s}…", img.first);
                if (err) {
                    spdlog::error("download error: {:s}",
                                  err->matrix_error.error);
                    downloaded = true;
                    return;
                }

                const std::string ext{fs::path(original_filename).extension()};
                const fs::path filename{tmpdir / global.opts.packid
                                        / (img.first + ext)};
                std::ofstream file(filename, std::ios::binary);
                file.write(data.data(),
                           static_cast<std::streamsize>(data.size()));

                if (file.good()) {
                    packfiles.emplace_back(filename);
                } else {
                    spdlog::error("write to {:s} failed", filename.c_str());
                }

                downloaded = true;
            });

        do {
            std::this_thread::sleep_for(std::chrono::milliseconds(60'000
                                                                  / opts.rate));
        } while (!downloaded);
    }

    nlohmann::json json(pack);
    const fs::path jsonfile{tmpdir / opts.packid / (opts.packid + ".json")};
    std::ofstream file(jsonfile);
    file << json.dump(4);
    file.close();
    packfiles.emplace_back(jsonfile);
    spdlog::debug("wrote event JSON: {:s}", jsonfile.c_str());

    global.client->close();

    archive::write(archive::format::zip, opts.output_file, packfiles,
                   tmpdir.c_str());
    fs::remove_all(tmpdir);
    spdlog::debug("removed temporary directory {:s}", tmpdir.c_str());
}

void upload(const options::options &opts) {
    namespace fs = std::filesystem;

    global.opts = opts;
    connect();

    while (!global.synced) {
        std::this_thread::sleep_for(100ms);
    }

    const auto extracted_pack{archive::extract(opts.input_file)};
    const auto directory{[&extracted_pack]() -> fs::path {
        for (const auto &entry : fs::directory_iterator(extracted_pack)) {
            if (fs::is_directory(entry)) {
                return entry.path();
            }
        }
        throw std::runtime_error{
            fmt::format("Could not find directory in {:s}", extracted_pack)};
    }()};
    const std::string oldname{directory.stem()};
    const auto jsonfile{directory / (directory.filename().string() + ".json")};
    spdlog::debug(fmt::format("found pack name: {:s}", oldname));
    spdlog::debug(fmt::format("reading JSON file: {:s}…", jsonfile.c_str()));
    nlohmann::json json{[&jsonfile]() {
        std::ifstream infile(jsonfile);
        return nlohmann::json::parse(infile);
    }()};
    json = json[0]; // FIXME: ???
    spdlog::debug("pack JSON: {:s}", json.dump(4));
    for (const auto &entry : fs::directory_iterator(directory)) {
        bool uploaded{false};
        if (entry.path() != jsonfile) {
            const auto filename{entry.path().filename()};
            spdlog::debug(fmt::format("found file: {:s}", filename.c_str()));
            std::ostringstream ss;
            {
                std::ifstream infile;
                infile.open(entry.path(), std::ios_base::binary);
                ss << infile.rdbuf();
            }
            const std::string mimetype{
                json["images"][filename.stem().c_str()]["info"]["mimetype"]};
            spdlog::debug("has mime type: {:s}", mimetype);
            global.client->upload(
                ss.str(), mimetype, filename,
                [filename, &uploaded,
                 &json](const mtx::responses::ContentURI &res,
                        mtx::http::RequestErr err) {
                    if (err) {
                        // maybe just log an error instead and skip/retry?
                        throw std::runtime_error{err->matrix_error.error};
                    }
                    spdlog::info("uploaded {:s}", filename.c_str());
                    spdlog::debug("to {:s}", res.content_uri);
                    json["images"][filename.stem().c_str()]
                        ["url"] = res.content_uri;
                    uploaded = true;
                });
            do {
                std::this_thread::sleep_for(
                    std::chrono::milliseconds(60'000 / opts.rate));
            } while (!uploaded);
        }
    }
    json["pack"]["display_name"] = opts.packid;
    json["pack"]["avatar_url"] = nullptr;

    bool received{false};
    global.client->send_state_event(
        opts.roomid, "im.ponies.room_emotes", opts.packid, json,
        [&received](const mtx::responses::EventId &,
                    mtx::http::RequestErr &err) {
            if (err) {
                throw std::runtime_error{fmt::format("error sending pack: {:s}",
                                                     err->matrix_error.error)};
                return;
            }
            received = true;
        });
    while (!received) {
        std::this_thread::sleep_for(100ms);
    }
    spdlog::info("pack uploaded as {:s}", opts.packid);

    global.client->close();
}

void connect() {
    mtx::utils::log::log()->set_level(global.opts.debug ? spdlog::level::debug
                                                        : spdlog::level::err);
    mtx::utils::log::log()->set_pattern("[%^%n%$] [%^%l%$] %v");

    auto &mxid = global.opts.mxid;
    std::string server{mxid.substr(mxid.find(':') + 1)};
    std::string user{mxid.substr(1, mxid.find(':') - 1)};
    global.client = std::make_shared<mtx::http::Client>(server);

    bool need_login{false};
    try {
        config::config cfg{config::read(global.opts.mxid)};
        global.client->set_device_id(cfg.device_id);
        global.client->set_access_token(cfg.access_token);
        global.connected = true;
    } catch (const std::exception &err) {
        spdlog::warn("could not read config: {:s}", err.what());
        need_login = true;
    }

    if (need_login) {
        // NOLINTNEXTLINE concurrency-mt-unsafe
        std::string password{getpass("password: ")};

        global.client->login(
            user, password,
            [](const mtx::responses::Login &res, mtx::http::RequestErr err) {
                if (err) {
                    throw std::runtime_error{fmt::format(
                        "error during login: {:s}", err->matrix_error.error)};
                    return;
                }

                spdlog::debug("logged in as {:s}", res.user_id.to_string());
                config::config cfg;
                cfg.device_id = res.device_id;
                cfg.access_token = res.access_token;
                spdlog::debug("set device ID and access token");
                config::write(global.opts.mxid, cfg);

                global.client->set_device_name(
                    global.client->device_id(), "mxstickertool",
                    [](const std::optional<mtx::http::ClientError> &cerr) {
                        if (cerr) {
                            spdlog::error("error setting device name: {:s}",
                                          cerr->matrix_error.error);
                        }
                    });
                global.connected = true;
            });
    }

    while (!global.connected) {
        std::this_thread::sleep_for(100ms);
    }
    spdlog::debug("logged in as device ID {:s}", global.client->device_id());

    global.client->sync({}, &initial_sync_handler);
}

void initial_sync_handler(const mtx::responses::Sync &res,
                          mtx::http::RequestErr err) {
    static std::uint8_t sync_error_counter{0};
    constexpr std::uint8_t max_tries{6};

    if (err) {
        spdlog::error("error during initial sync: {:s}",
                      err->matrix_error.error);
        ++sync_error_counter;

        if (err->status_code != 200 && sync_error_counter < max_tries) {
            spdlog::info("retrying initial sync…");
            std::this_thread::sleep_for(10s);
            global.client->sync({}, &initial_sync_handler);
        } else {
            throw std::runtime_error{"unrecoverable sync error"};
        }

        return;
    }

    if (!res.rooms.join.contains(global.opts.roomid)) {
        throw std::runtime_error{
            fmt::format("room {:s} is not joined", global.opts.roomid)};
        // TODO: join room
    }
    global.synced = true;
    spdlog::debug("inital sync complete");
}

} // namespace mxstickertool::matrix
