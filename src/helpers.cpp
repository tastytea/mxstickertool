// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "helpers.hpp"

#include <spdlog/spdlog.h>

#include <filesystem>
#include <string>

namespace mxstickertool::helpers {

std::filesystem::path make_tempdir() {
    std::string dirtemplate{std::filesystem::temp_directory_path()
                            / "mxstickertool-XXXXXX"};
    std::filesystem::path tmpdir{mkdtemp(dirtemplate.data())};
    spdlog::debug("created temporary directory {:s}", tmpdir.c_str());

    return tmpdir;
}

} // namespace mxstickertool::helpers
