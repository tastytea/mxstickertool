// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#ifndef MXSTICKERTOOL_OPTIONS_HPP
#define MXSTICKERTOOL_OPTIONS_HPP

#include <cstdint>
#include <string>

namespace mxstickertool::options {

enum class cmd {
    download,
    upload
};

struct options {
    std::string mxid;
    std::string roomid;
    std::string packid;
    std::uint16_t rate;
    cmd command;
    std::string output_file;
    std::string input_file;
    bool debug;
};

//! parse options and return them
[[nodiscard]] options parse_options(int argc, char *argv[]);

} // namespace mxstickertool::options

#endif
