// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#ifndef MXSTICKERTOOL_HELPERS_HPP
#define MXSTICKERTOOL_HELPERS_HPP

#include <filesystem>

namespace mxstickertool::helpers {

//! create temporary directory and return its path
std::filesystem::path make_tempdir();

} // namespace mxstickertool::helpers

#endif // MXSTICKERTOOL_HELPERS_HPP
