// SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

#ifndef MXSTICKERTOOL_ARCHIVE_HPP
#define MXSTICKERTOOL_ARCHIVE_HPP

#include <filesystem>
#include <string>
#include <string_view>
#include <vector>

namespace mxstickertool::archive {

// <https://antonyjr.in/QArchive/docs/QArchiveFormats.html>
enum class format : short { // NOLINT(google-runtime-int)
    zip = 201,
    targz = 205
};

/*! @brief write an archive to disk
 *  @param format   format of the archive
 *  @param filename archive filename
 *  @param files    files to put in archive
 *  @param basedir  directory that is stripped from archive entries
 */
void write(format format, std::string_view filename,
           const std::vector<std::filesystem::path> &files,
           std::string_view basedir);

//! return the path to the directory where the files are extracted
std::string extract(std::string_view filename);

} // namespace mxstickertool::archive

#endif // MXSTICKERTOOL_ARCHIVE_HPP
